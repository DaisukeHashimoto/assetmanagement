package jp.gnode.assetmanagement;

import java.util.HashMap;
import java.util.Map;

/**
 * データベースから取得した値の汎用格納型
 * <pre>
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
 */
public class CommonCommand {
	private Map<String,Object> properties = new HashMap<String, Object>();
	
	public Map<String,Object> getProperties() {
		return properties;
	}

	public void setProperties(Map<String,Object> properties) {
		this.properties = properties;
	}
}
