package jp.gnode.assetmanagement;

import java.util.Locale;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.gnode.assetmanagement.define.Define;

/**
 * 機材コレクションのドキュメント削除
 * <pre>
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
 */
@Controller
public class DeleteController {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * クライアントからのメソッドが不正の場合
	 * @param locale
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("move delete to list The client locale is {}.", locale);
		return "redirect:/list";
	}
	
	/**
	 * 機材コレクションのドキュメント削除
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param form
	 * @param model
	 * @param attributes
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String home(Locale locale, PclistIdModel form, Model model, RedirectAttributes attributes) {
		logger.info("post delete The client locale is {}.", locale);
		Query query = new Query();
		query.addCriteria(Criteria.where(Define.Field.KEY.toString()).is(form.getId()));
		
		mongoTemplate.remove(mongoTemplate.findById(new ObjectId((String)form.getId()),PclistIdModel.class));
		
		attributes.addFlashAttribute("result", "削除しました");
		return "redirect:/list";
	}
}
