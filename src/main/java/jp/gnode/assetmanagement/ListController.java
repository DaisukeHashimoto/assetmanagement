package jp.gnode.assetmanagement;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import jp.gnode.assetmanagement.define.Define;

/**
 * 機材コレクションの一覧表示
 * <pre>
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
 */
@Controller
public class ListController {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private String KEY = Define.Field.KEY.toString();
	private String PC = Define.Entity.PC;
	private String PC_COL = Define.Entity.PC_COL;
	private String ALIVE = Define.PC_COL_FIELD.ALIVE.toString();
	private String INDEX = Define.PC_COL_FIELD.INDEX.toString();
	private String PHYSICAL = Define.PC_COL_FIELD.PHYSICAL.toString();
	private String LOGICAL = Define.PC_COL_FIELD.LOGICAL.toString();
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * 機材コレクションの一覧表示
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param form
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public String home(Locale locale, CommonCommand form, Model model) {
		ObjectId objId = new ObjectId((String)form.getProperties().get(Define.Field.KEY.toString()));
		
		CommonCommand pc = new CommonCommand();
		{
			DBCollection pc_collection = mongoTemplate.getCollection(PC);
			DBObject pc_dbobj = pc_collection.findOne(new BasicDBObject(Define.Field.KEY.toString(), objId));
			if(pc_dbobj != null){
				@SuppressWarnings("unchecked")
				Map<String, Object> m = pc_dbobj.toMap();
				pc.setProperties(m);
			}
		}
		
		CommonCommand cc = new CommonCommand();
		Map<String,Object> disp = new HashMap<String,Object>();
		{
			Map<String,Object> map = new LinkedHashMap<String,Object>();
			
			DBCollection collection = mongoTemplate.getCollection(PC_COL);
			DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));
			while(cursor.hasNext()){
				final Map<?, ?> m = cursor.next().toMap();
				Object phy_key = m.get(PHYSICAL);
				final Object obj = pc.getProperties().get(phy_key != null ? phy_key : "");
				String str = (obj != null ? (String)obj : "");
				map.put(m.get(KEY).toString(), str);
				disp.put(m.get(KEY).toString(), m.get(LOGICAL));
			}
			cc.setProperties(map);
		}
		model.addAttribute("id", objId);
		model.addAttribute("commonCommand", cc);		
		model.addAttribute("disp", disp);		
		model.addAttribute("targetIdModel", new PclistIdModel());
		
		return "detail";
	}

	/**
	 * 機材コレクションの一覧表示
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome list! The client locale is {}.", locale);
		
		Map<String,String> tblHeader = new LinkedHashMap<String,String>();
		{
			DBCollection collection = mongoTemplate.getCollection(PC_COL);
			DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));
			while(cursor.hasNext()){
				final Map<?, ?> m = cursor.next().toMap();
				
				tblHeader.put((String)m.get(PHYSICAL),(String)m.get(LOGICAL));
			}
		}
		
		List<Map<String,Object>> pc = new LinkedList<Map<String,Object>>();
		{
			DBCollection pc_collection = mongoTemplate.getCollection(PC);
			DBCursor pc_cursor = pc_collection.find();
			while(pc_cursor.hasNext()){
				DBObject obj = pc_cursor.next();
				Map<String,Object> m = new HashMap<String,Object>();
				m.put(KEY, obj.get(KEY));
				for(String key : tblHeader.keySet() ){
					m.put(key, obj.get(key));
				}
				pc.add(m);
			}
		}
		model.addAttribute("tblHeader",tblHeader);
		model.addAttribute("all",pc);
		model.addAttribute("targetIdModel", new PclistIdModel());
		return "list";
	}
}
