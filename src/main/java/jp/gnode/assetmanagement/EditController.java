package jp.gnode.assetmanagement;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import jp.gnode.assetmanagement.define.Define;

/**
 * 機材コレクションの更新
 * <pre>
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
 */
@Controller
public class EditController {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private final String KEY = Define.Field.KEY.toString();
	private final String PC = Define.Entity.PC;
	private final String PC_COL = Define.Entity.PC_COL;
	
	private final String ALIVE = Define.PC_COL_FIELD.ALIVE.toString();
	private final String INDEX = Define.PC_COL_FIELD.INDEX.toString();
	private final String PHYSICAL = Define.PC_COL_FIELD.PHYSICAL.toString();
	private final String LOGICAL = Define.PC_COL_FIELD.LOGICAL.toString();
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * クライアントからのメソッドが不正の場合
	 * @param locale
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("move edit to list The client locale is {}.", locale);
		return "redirect:/list";
	}
	
	/**
	 * 機材コレクションの更新
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param form
	 * @param model
	 * @param attributes
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/edit", params="update2", method = RequestMethod.POST)
	public String home(Locale locale, CommonCommand form, Model model, RedirectAttributes attributes) {
		logger.info("post edit2 The client locale is {}.", locale);
		Map<String, Object> col_map = new HashMap<String, Object>();
		{
			DBCollection collection = mongoTemplate.getCollection(PC_COL);
			DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1));
			while(cursor.hasNext()){
				final Map<?,?> m = cursor.next().toMap();
				col_map.put(m.get(KEY).toString(), m.get(PHYSICAL));
			}
		}
		DBCollection collection = mongoTemplate.getCollection(PC);
		DBObject dbobj = collection.findOne(new BasicDBObject(KEY,new ObjectId((String)form.getProperties().get(KEY))));
		
		for(String key : form.getProperties().keySet() ){
			Object val = form.getProperties().get(key);
			if(key.equals(KEY)){
			}else{
				String logical = (String)col_map.get(key);
				dbobj.put((String)logical, val);
			}
		}
		collection.save(dbobj);
		
		attributes.addFlashAttribute("result", "更新しました。");
		
		return "redirect:/list";
	}
	
	/**
	 * 機材コレクションのドキュメントの編集画面表示
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param form
	 * @param model
	 * @param attributes
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String home(Locale locale, PclistIdModel form, Model model) {
		logger.info("post edit targetID The client locale is {}.", locale);
		
		CommonCommand pc = new CommonCommand();
		{
			DBCollection pc_collection = mongoTemplate.getCollection(PC);
			DBObject pc_dbobj = pc_collection.findOne(new BasicDBObject(KEY,new ObjectId(form.getId())));
			if(pc_dbobj != null){
				@SuppressWarnings("unchecked")
				Map<String, Object> map = pc_dbobj.toMap();
				pc.setProperties(map);
			}
		}
		
		CommonCommand cc = new CommonCommand();
		Map<String,Object> disp = new HashMap<String,Object>();
		{
			Map<String,Object> map = new LinkedHashMap<String,Object>();
			
			DBCollection collection = mongoTemplate.getCollection(PC_COL);
			DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));
			while(cursor.hasNext()){
				final Map<?, ?> m = cursor.next().toMap();
				Object phy_key = m.get(PHYSICAL);
				final Object obj = pc.getProperties().get(phy_key != null ? phy_key : "");
				String str = (obj != null ? (String)obj : "");
				map.put(m.get(KEY).toString(), str);
				disp.put(m.get(KEY).toString(), m.get(LOGICAL));
			}
			cc.setProperties(map);
		}
		model.addAttribute("id", form.getId());
		model.addAttribute("commonCommand", cc);		
		model.addAttribute("disp", disp);		
		
		return "edit";
	}
}
