package jp.gnode.assetmanagement;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import jp.gnode.assetmanagement.define.Define;

/**
 * 機材コレクションのドキュメント追加
 * <pre>
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
 */
@Controller
public class InsertController {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private String KEY = Define.Field.KEY.toString();
	private String PC = Define.Entity.PC;
	private String PC_COL = Define.Entity.PC_COL;
	private String ALIVE = Define.PC_COL_FIELD.ALIVE.toString();
	private String INDEX = Define.PC_COL_FIELD.INDEX.toString();
	private String PHYSICAL = Define.PC_COL_FIELD.PHYSICAL.toString();
	private String LOGICAL = Define.PC_COL_FIELD.LOGICAL.toString();
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * 機材コレクションのドキュメント追加入力画面表示
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/insert", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome insert! The client locale is {}.", locale);
		
		CommonCommand pc = new CommonCommand();
		CommonCommand cc = new CommonCommand();
		Map<String,Object> disp = new HashMap<String,Object>();
		{
			Map<String,Object> map = new LinkedHashMap<String,Object>();
			
			DBCollection collection = mongoTemplate.getCollection(PC_COL);
			DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));
			while(cursor.hasNext()){
				final Map<?, ?> m = cursor.next().toMap();
				Object phy_key = m.get(PHYSICAL);
				final Object obj = pc.getProperties().get(phy_key != null ? phy_key : "");
				String str = (obj != null ? (String)obj : "");
				map.put(m.get(KEY).toString(), str);
				disp.put(m.get(KEY).toString(), m.get(LOGICAL));
			}
			cc.setProperties(map);
		}
		model.addAttribute("commonCommand", cc);		
		model.addAttribute("disp", disp);		
		
		return "insert";
	}
	
	/**
	 * 機材コレクションのドキュメント追加
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param form
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/insert", params=Define.RequestMappingParams.PC_INSERT, method = RequestMethod.POST)
	public String home(Locale locale, CommonCommand form, Model model) {
		logger.info("post insert2 The client locale is {}.", locale);
		
		Map<String, Object> col_map = new HashMap<String, Object>();
		{
			DBCollection collection = mongoTemplate.getCollection(PC_COL);
			DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1));
			while(cursor.hasNext()){
				final Map<?, ?> m = cursor.next().toMap();
				col_map.put(m.get(KEY).toString(), m.get(PHYSICAL));
			}
		}
		
		BasicDBObject dbobj = new BasicDBObject();
		for(String key : form.getProperties().keySet() ){
			Object val = form.getProperties().get(key);
			String logical = (String)col_map.get(key);
			dbobj.append((String)logical, val);
		}
		DBCollection collection = mongoTemplate.getCollection(PC);
		collection.insert(dbobj);
		
		CommonCommand pc = new CommonCommand();
		CommonCommand cc = new CommonCommand();
		Map<String,Object> disp = new HashMap<String,Object>();
		{
			Map<String,Object> map = new LinkedHashMap<String,Object>();
			
			DBCollection col_collection = mongoTemplate.getCollection(PC_COL);
			DBCursor cursor = col_collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));
			while(cursor.hasNext()){
				final Map<?, ?> m = cursor.next().toMap();
				Object phy_key = m.get(PHYSICAL);
				final Object obj = pc.getProperties().get(phy_key != null ? phy_key : "");
				String str = (obj != null ? (String)obj : "");
				map.put(m.get(KEY).toString(), str);
				disp.put(m.get(KEY).toString(), m.get(LOGICAL));
			}
			cc.setProperties(map);
		}
		model.addAttribute("commonCommand", cc);		
		model.addAttribute("disp", disp);		

		model.addAttribute("result","追加しました");
		return "insert";
	}
}
