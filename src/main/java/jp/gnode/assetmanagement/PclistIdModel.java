package jp.gnode.assetmanagement;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jp.gnode.assetmanagement.define.Define;

/**
 * 機材エントリーのドキュメント一意特定キー
 * <pre>
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
　*/
@Document(collection = Define.Entity.PC)
public class PclistIdModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
