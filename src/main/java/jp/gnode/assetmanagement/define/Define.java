package jp.gnode.assetmanagement.define;

/**
 * 定数の定義
 * <pre>
 * 定数はここでまとめる
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
 */
public final class Define {
	private Define(){}
	
	/**
	 * プロパティ一覧
	 * <pre>
	 * DBのプロパティを列挙
	 * </pre>
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	public static enum Field {
		KEY("_id");
		
		private String value = null;
		
		Field(String s) {
			this.value = s;
		}
		
		@Override
		public String toString() {
			return value;
		}
	}
	
	/**
	 * コレクション一覧
	 * <pre>
	 * DBのコレクションを列挙
	 * </pre>
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	public final class Entity {
		// アノテーション内の値はenumは難しい。。。
		private Entity(){}
		
		/** 機材 */
		public final static String PC = "pc";
		
		/** 項目 */
		public final static String PC_COL = "pc_col";
	}
	
	/**
	 * PC_COLのプロパティ一覧
	 * <pre>
	 * </pre>
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	public static enum PC_COL_FIELD {
		KEY("_id"),
		REQUIRED("required"),
		ALIVE("alive"),
		INDEX("index"),
		PHYSICAL("physical"),
		LOGICAL("logical");
		
		private String value = null;
		
		PC_COL_FIELD(String s) {
			this.value = s;
		}
		
		@Override
		public String toString() {
			return value;
		}
	}
	
	/**
	 * RequestMapping一覧
	 * <pre>
	 * 使用しているRequestMappingを列挙
	 * </pre>
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	public final class RequestMappingParams {
		private RequestMappingParams(){}
		
		public final static String COL_CHANGE = "change";
		public final static String COL_DELETE = "delete";
		public final static String PC_INSERT = "insert2";
	}
}
