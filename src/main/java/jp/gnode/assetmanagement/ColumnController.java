package jp.gnode.assetmanagement;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import jp.gnode.assetmanagement.define.Define;

/**
 * 項目コレクションの操作
 * <pre>
 * </pre>
 * @author g-node
 * @version 0.1
 * @since 0.1
 */
@Controller
public class ColumnController {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	final String KEY		= Define.PC_COL_FIELD.KEY.toString();
	final String REQUIRED	= Define.PC_COL_FIELD.REQUIRED.toString();
	final String ALIVE		= Define.PC_COL_FIELD.ALIVE.toString();
	final String INDEX		= Define.PC_COL_FIELD.INDEX.toString();
	final String PHYSICAL	= Define.PC_COL_FIELD.PHYSICAL.toString();
	final String LOGICAL	= Define.PC_COL_FIELD.LOGICAL.toString();
	
	/**
	 * 項目コレクションのドキュメントの表示順序の入れ替え
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param form
	 * @param model
	 * @return 表示ビュー名
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/column", params=Define.RequestMappingParams.COL_CHANGE, method = RequestMethod.POST)
	public String change(Locale locale, CommonCommand form,  Model model) {
		
		logger.info("Welcome column change! The client locale is {}.", locale);
		
		DBCollection collection = mongoTemplate.getCollection(Define.Entity.PC_COL);
		{
			DBObject id0 = collection.findOne(new BasicDBObject(KEY,new ObjectId((String)form.getProperties().get("0"))));
			DBObject id1 = collection.findOne(new BasicDBObject(KEY,new ObjectId((String)form.getProperties().get("1"))));
			
			Integer id0Index = (Integer)id0.toMap().get(INDEX);
			Integer id1Index = (Integer)id1.toMap().get(INDEX);
			// 値を入れ替え
			id0.put(INDEX, id1Index);
			id1.put(INDEX, id0Index);
			
			collection.save(id0);
			collection.save(id1);
		}
		DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));;
		List<Map<?,?>> list = new LinkedList<Map<?,?>>();
		while(cursor.hasNext()){
			list.add( cursor.next().toMap());
		}
		
		model.addAttribute("list", list);
		CommonCommand cc = new CommonCommand();
		cc.setProperties(new HashMap<String,Object>(){
			private static final long serialVersionUID = 1L;
			{put(Define.PC_COL_FIELD.INDEX.toString(),"");}
			{put(PHYSICAL,"");}
			{put(LOGICAL,"");}
		});
		model.addAttribute(cc);
		
		return "column";
	}
	
	/**
	 * 項目コレクションのドキュメント削除
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param form
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/column", params=Define.RequestMappingParams.COL_DELETE, method = RequestMethod.POST)
	public String delete(Locale locale, CommonCommand form,  Model model) {
		logger.info("Welcome column change! The client locale is {}.", locale);
		
		DBCollection collection = mongoTemplate.getCollection(Define.Entity.PC_COL);
		{
			DBObject id = collection.findOne(new BasicDBObject(KEY,new ObjectId((String)form.getProperties().get(KEY))));
			id.put(ALIVE, 0);
			collection.save(id);
		}
		DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));;
		List<Map<?,?>> list = new LinkedList<Map<?,?>>();
		while(cursor.hasNext()){
			list.add( cursor.next().toMap());
		}
		
		model.addAttribute("list", list);
		CommonCommand cc = new CommonCommand();
		cc.setProperties(new HashMap<String,Object>(){
			private static final long serialVersionUID = 1L;
			{put(Define.PC_COL_FIELD.INDEX.toString(),"");}
			{put(PHYSICAL,"");}
			{put(LOGICAL,"");}
		});
		model.addAttribute(cc);
		
		return "column";
	}
	
	/**
	 * 項目コレクションのドキュメントの追加
	 * <pre>
	 * </pre>
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 * @param locale
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/column", method = RequestMethod.POST)
	public String home(Locale locale, CommonCommand form,  Model model) {
		logger.info("Welcome column! The client locale is {}.", locale);
		DBCollection collection = mongoTemplate.getCollection(Define.Entity.PC_COL);
		{
			String resulttext = "重複する値があります";
			
			BasicDBList or = new BasicDBList();
			CommonCommand cc = new CommonCommand();
			Integer index = 1;
			try{
				index = Integer.parseInt((String)form.getProperties().get(INDEX));
			} catch (NumberFormatException e){
				logger.error("error:{}",e);
				resulttext = "順序が未入力です";
			}
			or.add(new BasicDBObject(Define.PC_COL_FIELD.INDEX.toString(), index));
			or.add(new BasicDBObject(PHYSICAL, form.getProperties().get(PHYSICAL)));
			DBObject exist_query = new BasicDBObject("$or", or);
			DBCursor cursor = collection.find(exist_query);
			if(cursor.hasNext()){
				// 重複確認あり
				DBObject dbobj = cursor.next();
				Map<?, ?> matchobj = dbobj.toMap();
				if((Integer)matchobj.get(ALIVE) == 0){
					resulttext = "削除済項目内に重複する値があります";
				}
				
				Map<String,Object> m = new HashMap<String,Object>();
				m.put(INDEX,(String)form.getProperties().get(INDEX));
				String s = (String)form.getProperties().get(PHYSICAL);
				m.put(PHYSICAL,s != null ? s : "");
				s = (String)form.getProperties().get(LOGICAL);
				m.put(LOGICAL,s != null ? s : "");
				cc.setProperties(m);
				model.addAttribute(cc);
				model.addAttribute("result",resulttext);
			} else {
				Map<String,Object> map = form.getProperties();
				DBObject dbobj = new BasicDBObject()
						.append(REQUIRED, 0                           )
						.append(ALIVE   , 1                           )
						.append(INDEX   , index                       )
						.append(PHYSICAL, map.get(PHYSICAL).toString())
						.append(LOGICAL , map.get(LOGICAL).toString() );
				collection.insert(dbobj);

				cc.setProperties(new HashMap<String,Object>(){
					private static final long serialVersionUID = 1L;
					{put(INDEX,"");}
					{put(PHYSICAL,"");}
					{put(LOGICAL,"");}
				});
				model.addAttribute(cc);
				model.addAttribute("result","追加しました");
			}
		}
		
		{
			DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));;
			List<Map<?,?>> list = new LinkedList<Map<?,?>>();
			while(cursor.hasNext()){
				list.add( cursor.next().toMap());
			}
			model.addAttribute("list", list);
		}
		return "column";
	}
	
	/**
	 * 項目コレクションの編集画面表示
	 * <pre>
	 * </pre>
	 * @param locale
	 * @param model
	 * @return
	 * @author g-node
	 * @version 0.1
	 * @since 0.1
	 */
	@RequestMapping(value = "/column", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome column! The client locale is {}.", locale);
		
		DBCollection collection = mongoTemplate.getCollection(Define.Entity.PC_COL);
		DBCursor cursor = collection.find(new BasicDBObject(ALIVE,1)).sort(new BasicDBObject(INDEX,1));;
		List<Map<?,?>> list = new LinkedList<Map<?,?>>();
		while(cursor.hasNext()){
			list.add( cursor.next().toMap());
		}
		
		model.addAttribute("list", list);
		CommonCommand cc = new CommonCommand();
		cc.setProperties(new HashMap<String,Object>(){
			private static final long serialVersionUID = 1L;
			{put(INDEX,"");}
			{put(PHYSICAL,"");}
			{put(LOGICAL,"");}
		});
		model.addAttribute(cc);
		return "column";
	}

}
