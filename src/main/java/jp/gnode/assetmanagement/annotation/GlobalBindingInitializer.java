package jp.gnode.assetmanagement.annotation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.context.request.WebRequest;
import jp.gnode.assetmanagement.HomeController;
import org.springframework.web.bind.support.WebBindingInitializer;

public class GlobalBindingInitializer implements WebBindingInitializer {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Override
	public void initBinder(WebDataBinder binder, WebRequest req) {
		logger.info("load", "");
		
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));	
	}
}
