<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf8">
<title>Insert title here</title>
</head>
<body>
<h1>PC資産管理</h1>
<p><c:out value="${result}" /></p>
<a href="./list">一覧</a> <a href="./insert">追加</a> <a href="./column">項目</a>

<table border="1">
<tr>
<c:forEach var="itm" items="${tblHeader}">
<th><c:out value="${itm.value}" /></th>
</c:forEach>
<th colspan="2"></th>
</tr>

<c:forEach var="elm" items="${all}">
<tr>
<c:forEach var="itm" items="${tblHeader}">
<td><c:out value="${elm[itm.key]}" /></td>
</c:forEach>
<td>
<form:form action="edit" method="post" modelAttribute="targetIdModel">
<form:hidden path="id" value="${elm['_id']}"></form:hidden>
<input type="submit" value="編集">
</form:form>
</td>
<td>
<form:form action="delete" method="post" modelAttribute="targetIdModel" onsubmit="return confirm('削除しますか')">
<form:hidden path="id" value="${elm['_id']}"></form:hidden>
<input type="submit" value="削除">
</form:form>
</td>
</tr>
</c:forEach>
</table>

</body>
</html>