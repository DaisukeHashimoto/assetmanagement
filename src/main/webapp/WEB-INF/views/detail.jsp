<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf8">
<title>Insert title here</title>
</head>
<body>
<h1>PC資産管理</h1>
<a href="./list">一覧</a> <a href="./insert">追加</a> <a href="./column">項目</a>

<table border="1">
<c:forEach var="itm" items="${commonCommand.properties}">
<tr>
<td><c:out value="${disp[itm.key]}"/></td>
<td><c:out value="${itm.value}" /></td>
</tr>
</c:forEach>
<tr><td colspan="2">

<table>
<tr><td>
<form:form action="edit" method="post" modelAttribute="assetModel">
<form:hidden path="id" value="${id}"></form:hidden>
<input type="submit" value="編集">
</form:form>
</td><td>
<form:form action="delete" method="post" modelAttribute="assetModel" onsubmit="return confirm('削除しますか')">
<form:hidden path="id" value="${id}"></form:hidden>
<input type="submit" value="削除">
</form:form>
</td></tr>
</table>

</td></tr>
</table>

</body>
</html>