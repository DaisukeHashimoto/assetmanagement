<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="jp.gnode.assetmanagement.define.Define"%>
<%
pageContext.setAttribute("index",Define.PC_COL_FIELD.INDEX.toString()); 
pageContext.setAttribute("physical",Define.PC_COL_FIELD.PHYSICAL.toString()); 
pageContext.setAttribute("logical",Define.PC_COL_FIELD.LOGICAL.toString()); 
%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf8">
<title>Insert title here</title>
</head>
<body>
<h1>PC資産管理</h1>
<p><c:out value="${result}" /></p>
<a href="./list">一覧</a> <a href="./insert">追加</a> <a href="./column">項目</a>
<table border="1">
<tr>
<th>物理名</th>
<th>論理名</th>
<th>順序</th>
<th colspan="3">&nbsp;</th>
</tr>
<c:forEach var="itm" items="${list}">
<tr>
<td>
<c:out value="${itm[physical]}" />
</td>
<td>
<c:out value="${itm[logical]}" />
</td>
<td>
<c:out value="${itm[index]}" />
</td>
<td>
<c:if test="${!empty bef}">
<form:form action="column" method="post" modelAttribute="commonCommand">
<form:hidden path="properties['0']" value="${itm['_id']}"/>
<form:hidden path="properties['1']" value="${bef['_id']}"/>
<input type="submit" name="<%= Define.RequestMappingParams.COL_CHANGE %>" value="↑" />
</form:form>
</c:if>
<c:set var="bef" value="${itm}"/>
</td>
<td>
<form:form action="column" method="post" modelAttribute="commonCommand">
<form:hidden path="properties['_id']" value="${itm['_id']}"/>
<input type="submit" name="<%= Define.RequestMappingParams.COL_DELETE %>" value="削除" />
</form:form>
</td>
</tr>
</c:forEach>
<tr>
<form:form action="column" method="post" modelAttribute="commonCommand">
<td>
<form:input path="properties[physical]"/>
</td>
<td>
<form:input path="properties[logical]"/>
</td>
<td>
<form:input type="number" path="properties[index]"/>
</td>
<td>
<input type="submit" value="追加" />
</td>
</form:form>
</tr>
</table>
</body>
</html>