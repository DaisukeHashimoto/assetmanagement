<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="jp.gnode.assetmanagement.define.Define"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf8">
<title>Insert title here</title>
</head>
<body>
<h1>PC資産管理</h1>
<p><c:out value="${result}" /></p>
<a href="./list">一覧</a> <a href="./insert">追加</a> <a href="./column">項目</a>

<table border="1">
<form:form action="insert" method="post" modelAttribute="commonCommand">
<c:forEach var="itm" items="${commonCommand.properties}">
<tr>
<td><form:label path="properties['${itm.key}']"><c:out value="${disp[itm.key]}"/></form:label></td>
<td>
<form:input path="properties['${itm.key}']" />
</td>
</tr>
</c:forEach>
<tr><td colspan="2">
<input type="submit" name="<%= Define.RequestMappingParams.PC_INSERT %>" value="追加" />
</td></tr>
</form:form>
</table>

</body>
</html>