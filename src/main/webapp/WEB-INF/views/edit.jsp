<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-typ" content="text/html; charset=utf8">
<title>Insert title here</title>
</head>
<body>
<h1>PC資産管理</h1>
<a href="./list">一覧</a> <a href="./insert">追加</a> <a href="./column">項目</a>
<p><c:out value="${result}" /></p>

<table border="1">
<form:form action="edit" method="post" modelAttribute="commonCommand">
<form:hidden path="properties['_id']" value="${id}"/>
<c:forEach var="itm" items="${commonCommand.properties}">
<tr>
<td><form:label path="properties['${itm.key}']"><c:out value="${disp[itm.key]}"/></form:label></td>
<td>
<form:input path="properties['${itm.key}']" />
</td>
</tr>
</c:forEach>
<tr><td colspan="2">
<input type="submit" name="update2" value="更新" />
</td></tr>
</form:form>
</table>
</body>
</html>